# py_web_hello

A simple python web application run by `Flask`

# docker-tutorial

Docker 基本教學 - 從無到有 Docker-Beginners-Guide  
https://github.com/twtrubiks/docker-tutorial


# Build app

```sh
docker build -t py_web_hello .
```

# Run app

```sh
docker run --rm -d -p 4000:80 py_web_hello
```

# Stop app

```sh
docker stop ${CONTAINER_ID}
```

# Publish to Docker Hub

```sh
docker login
docker tag py_web_hello ${YOUR_DOCKERHUB_NAME}/py_web_hello
docker push ${YOUR_DOCKERHUB_NAME}/py_web_hello
```

# Reference
 - https://blog.gtwang.org/virtualization/docker-basic-tutorial/2/
